/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 00:14:24 by bbaelor-          #+#    #+#             */
/*   Updated: 2019/02/27 05:28:03 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void		count_line2(t_mlx *mlx, int fd)
{
	int		i;
	int		k;
	char	*s;

	if (fd < 0)
		ft_exit("Bad map(\n");
	i = 0;
	while ((k = get_next_line(fd, &s)) > 0)
		mlx->nbr_line++;
	close(fd);
}

void		count_line(t_fdf **arr, t_mlx *mlx)
{
	int		i;

	i = 0;
	while (arr[i])
	{
		i++;
	}
	mlx->nbr_line = i;
}

int			tab_len(char **tab, t_mlx *mlx)
{
	int i;
	int res;

	res = 0;
	i = 0;
	while (tab[i])
	{
		i++;
		res++;
	}
	mlx->len_line = res;
	return (res);
}

void		check_valid(t_mlx *fdf, char **str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	if (fdf->check_len == -1)
		fdf->check_len = i;
	else if (fdf->check_len != i)
		ft_exit("Wrong map\n");
}

void		check_valid2(int argc, char **argv)
{
	int fd;

	if (argc < 2)
		ft_exit("Usage: ./fdf <map_filename>\n");
	if (argc == 3 || argc > 4)
		ft_exit("Usage : ./fdf <filename> [ case_size z_size ]\n");
	if ((fd = open(argv[1], O_RDONLY)) < 0)
		ft_exit("Bad map(\n");
	if (read(fd, 0, 0) < 0)
		ft_exit("directory, really?\n");
}
