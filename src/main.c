/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/06 02:02:51 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:27:46 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void		ft_exit(char *str)
{
	ft_putstr(str);
	exit(0);
}

t_fdf		*char_to_struct(char **tab, int j, t_mlx *mlx)
{
	t_fdf	*res_tab;
	int		i;
	char	*s;

	i = 0;
	res_tab = (t_fdf*)malloc(sizeof(t_fdf) * (1 + tab_len(tab, mlx)));
	while (tab[i])
	{
		res_tab[i].z = ft_atoi(tab[i]);
		res_tab[i].color = 0xFFFFFF;
		while (*tab[i])
		{
			if (*tab[i] == ',')
			{
				tab[i] += 3;
				res_tab[i].color = ft_atoi_base(tab[i], 16);
			}
			tab[i]++;
		}
		res_tab[i].y = j;
		res_tab[i].x = i;
		i++;
	}
	return (res_tab);
}

t_fdf		**set_global_matrix(int argc, char **argv, t_mlx *fdf)
{
	int		i;
	int		fd;
	char	**buf;
	char	**str;

	count_line2(fdf, open(argv[1], O_RDONLY));
	fdf->tab = (t_fdf **)malloc(sizeof(t_fdf *) * 9999);
	fdf->cpy_tab = (t_fdf **)malloc(sizeof(t_fdf *) * 9999);
	check_valid2(argc, argv);
	i = 0;
	fd = open(argv[1], O_RDONLY);
	str = malloc(sizeof(char *) * 99999);
	fdf->check_len = -1;
	while (get_next_line(fd, &str[i]) > 0)
	{
		buf = ft_strsplit(str[i], ' ');
		check_valid(fdf, buf);
		fdf->tab[i] = char_to_struct(buf, i, fdf);
		fdf->cpy_tab[i] = malloc(sizeof(t_mlx) * fdf->len_line);
		ft_memcpy(fdf->cpy_tab[i], fdf->tab[i], sizeof(t_mlx) * fdf->len_line);
		i++;
	}
	free(str);
	return (fdf->tab);
}

t_mlx		*ft_mlx_sheet(int argc, char **argv)
{
	t_mlx	*mlx;

	mlx = (t_mlx *)malloc(sizeof(t_mlx));
	mlx->tab = set_global_matrix(argc, argv, mlx);
	mlx->move_down = 0;
	mlx->move_up = 0;
	mlx->move_left = 0;
	mlx->move_right = 0;
	mlx->perspect = 0;
	count_line(mlx->tab, mlx);
	if (mlx->len_line > mlx->nbr_line)
		mlx->scale = 1000 / mlx->len_line;
	else
		mlx->scale = 600 / mlx->nbr_line;
	return (mlx);
}

int			main(int argc, char **argv)
{
	t_mlx	*mlx;
	int		k;
	int		l;

	mlx = ft_mlx_sheet(argc, argv);
	k = 1920;
	l = 1080;
	if (argc == 4)
	{
		k = atoi(argv[2]);
		l = atoi(argv[3]);
	}
	mlx->mlx = mlx_init();
	mlx->win = mlx_new_window(mlx->mlx, k, l, "krya");
	mlx->x_centr = (mlx->len_line * mlx->scale + 10) / 2;
	mlx->y_centr = (mlx->nbr_line * mlx->scale + 10) / 2;
	ft_drow(mlx->tab, mlx);
	mlx_hook(mlx->win, 2, 0, press, mlx);
	mlx_loop(mlx->mlx);
	return (0);
}
