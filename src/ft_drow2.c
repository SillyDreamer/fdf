/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_drow2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 17:36:09 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:30:17 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void		configure_output(t_fdf **fdf, t_mlx *mlx)
{
	if (mlx->perspect == 0)
		ft_drow(fdf, mlx);
	if (mlx->perspect == 1)
		ft_drow2(fdf, mlx);
}

t_fdf		ft_scale2(t_fdf v, t_mlx *mlx)
{
	t_fdf	z;
	float	previous_x;
	float	previous_y;

	previous_x = v.x;
	previous_y = v.y;
	v.x = (previous_x - previous_y) * cos(0.523599);
	v.y = -v.z + (previous_x + previous_y) * sin(0.523599);
	v.x *= mlx->scale;
	v.y *= mlx->scale;
	z.x = (int)v.x;
	z.y = (int)v.y;
	return (z);
}

void		ft_drow2(t_fdf **fdf, t_mlx *mlx)
{
	int		i;
	int		j;

	i = 0;
	while (i < mlx->nbr_line)
	{
		j = 0;
		while (j < mlx->len_line)
		{
			if (i + 1 < mlx->nbr_line)
				ft_putline(ft_scale2(fdf[i][j], mlx),
				ft_scale2(fdf[i + 1][j], mlx), mlx, fdf[i][j].color);
			if (j + 1 < mlx->len_line)
				ft_putline(ft_scale2(fdf[i][j], mlx), ft_scale2(fdf[i][j + 1],
				mlx), mlx, fdf[i][j].color);
			j++;
		}
		i++;
	}
}
