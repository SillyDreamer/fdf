/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 17:44:42 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:29:19 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void	press4(int button, t_mlx *map, int i, int j)
{
	if (button == 6)
	{
		i = 0;
		while (i < map->nbr_line)
		{
			j = 0;
			while (j < map->len_line)
			{
				map->cpy_tab[i][j].x = map->tab[i][j].x;
				map->cpy_tab[i][j].y = map->tab[i][j].y;
				map->cpy_tab[i][j].z = map->tab[i][j].z;
				j++;
			}
			i++;
		}
		mlx_clear_window(map->mlx, map->win);
		rot_x(0, map);
		rot_y(0, map);
		rot_z(0.05, map);
		configure_output(map->cpy_tab, map);
	}
}

void	press3(int button, t_mlx *map, int i, int j)
{
	if (button == 16)
	{
		i = 0;
		while (i < map->nbr_line)
		{
			j = 0;
			while (j < map->len_line)
			{
				map->cpy_tab[i][j].x = map->tab[i][j].x;
				map->cpy_tab[i][j].y = map->tab[i][j].y;
				map->cpy_tab[i][j].z = map->tab[i][j].z;
				j++;
			}
			i++;
		}
		mlx_clear_window(map->mlx, map->win);
		rot_x(0, map);
		rot_y(0.05, map);
		rot_z(0, map);
		configure_output(map->cpy_tab, map);
	}
}

void	press2(int button, t_mlx *map, int i, int j)
{
	if (button == 12)
		mlx_clear_window(map->mlx, map->win);
	if (button == 7)
	{
		i = 0;
		while (i < map->nbr_line)
		{
			j = 0;
			while (j < map->len_line)
			{
				map->cpy_tab[i][j].x = map->tab[i][j].x;
				map->cpy_tab[i][j].y = map->tab[i][j].y;
				map->cpy_tab[i][j].z = map->tab[i][j].z;
				j++;
			}
			i++;
		}
		mlx_clear_window(map->mlx, map->win);
		rot_x(0.05, map);
		rot_y(0, map);
		rot_z(0, map);
		configure_output(map->cpy_tab, map);
	}
}

void	press_caller(int button, t_mlx *map, int i, int j)
{
	press2(button, map, i, j);
	press3(button, map, i, j);
	press4(button, map, i, j);
	press5(button, map, i, j);
	press6(button, map, i, j);
	press7(button, map, i, j);
	press8(button, map, i, j);
	press9(button, map, i, j);
}

int		press(int button, t_mlx *map)
{
	int		i;
	int		j;

	press_caller(button, map, i, j);
	if (button == 24)
	{
		mlx_clear_window(map->mlx, map->win);
		map->scale += 1;
		configure_output(map->cpy_tab, map);
	}
	if (button == 33)
	{
		mlx_clear_window(map->mlx, map->win);
		ft_drow(map->tab, map);
	}
	if (button == 27)
	{
		mlx_clear_window(map->mlx, map->win);
		map->scale -= 1;
		configure_output(map->cpy_tab, map);
	}
	return (0);
}
