/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putline.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 17:45:54 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:28:41 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

#define ABS(x) (x < 0) ? -(x) : x

void		ft_kost_mas(int *mas, int a, int b)
{
	mas[0] = a;
	mas[1] = b;
}

void		ft_putline(t_fdf fdf, t_fdf fdf2, t_mlx *mlx, int color)
{
	int			delta[2];
	int			sign[2];
	int			errors[2];

	ft_kost_mas(delta, ABS(fdf2.x - fdf.x), ABS(fdf2.y - fdf.y));
	ft_kost_mas(sign, (fdf.x < fdf2.x ? 1 : -1), ((fdf.y < fdf2.y ? 1 : -1)));
	errors[0] = delta[0] - delta[1];
	while (fdf.x != fdf2.x || fdf.y != fdf2.y)
	{
		mlx_pixel_put(mlx->mlx, mlx->win, (int)fdf.x - mlx->move_left +
		mlx->move_right, (int)fdf.y + mlx->move_down - mlx->move_up, color);
		errors[1] = errors[0] * 2;
		if (errors[1] > -delta[1])
		{
			errors[0] -= delta[1];
			fdf.x += sign[0];
		}
		if (errors[1] < delta[0])
		{
			errors[0] += delta[0];
			fdf.y += sign[1];
		}
	}
	mlx_pixel_put(mlx->mlx, mlx->win, fdf2.x - mlx->move_left +
	mlx->move_right, fdf2.y + mlx->move_down - mlx->move_up, color);
}

t_fdf		ft_scale(t_fdf v, t_mlx *mlx)
{
	t_fdf	z;

	z.color = 0xFF0000;
	v.x *= mlx->scale;
	v.y *= mlx->scale;
	z.x = (int)v.x;
	z.y = (int)v.y;
	return (z);
}

void		ft_drow(t_fdf **fdf, t_mlx *mlx)
{
	int		i;
	int		j;

	i = 0;
	while (i < mlx->nbr_line)
	{
		j = 0;
		while (j < mlx->len_line)
		{
			if (i + 1 < mlx->nbr_line)
				ft_putline(ft_scale(fdf[i][j], mlx),
				ft_scale(fdf[i + 1][j], mlx), mlx, fdf[i][j].color);
			if (j + 1 < mlx->len_line)
				ft_putline(ft_scale(fdf[i][j], mlx),
				ft_scale(fdf[i][j + 1], mlx), mlx, fdf[i][j].color);
			j++;
		}
		i++;
	}
}
