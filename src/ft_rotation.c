/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotation.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 15:40:39 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:29:49 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void	centr(t_mlx *map)
{
	int	i;
	int	j;

	i = 0;
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->cpy_tab[i][j].x -= map->len_line / 2;
			map->cpy_tab[i][j].y -= map->nbr_line / 2;
			j++;
		}
		i++;
	}
}

void	centr2(t_mlx *map)
{
	int	i;
	int	j;

	i = 0;
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->cpy_tab[i][j].x += map->len_line / 2;
			map->cpy_tab[i][j].y += map->nbr_line / 2;
			j++;
		}
		i++;
	}
}

void	rot_x(double x, t_mlx *map)
{
	int		i;
	int		j;

	map->x_pow += x;
	if (map->x_pow > 6.283185307)
		map->x_pow -= 6.283185307;
	x = map->x_pow;
	centr(map);
	i = 0;
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->cpy_tab[i][j].y = map->cpy_tab[i][j].y * cos(x) +
			map->cpy_tab[i][j].z * sin(x);
			map->cpy_tab[i][j].z = -map->cpy_tab[i][j].y * sin(x) +
			map->cpy_tab[i][j].z * cos(x);
			j++;
		}
		i++;
	}
	centr2(map);
}

void	rot_y(double x, t_mlx *map)
{
	int		i;
	int		j;

	i = 0;
	map->y_pow += x;
	if (map->y_pow > 6.283185307)
		map->y_pow -= 6.283185307;
	x = map->y_pow;
	centr(map);
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->cpy_tab[i][j].x = map->cpy_tab[i][j].x * cos(x) +
			map->cpy_tab[i][j].z * sin(x);
			map->cpy_tab[i][j].z = -map->cpy_tab[i][j].x * sin(x) +
			map->cpy_tab[i][j].z * cos(x);
			j++;
		}
		i++;
	}
	centr2(map);
}

void	rot_z(double x, t_mlx *map)
{
	int		i;
	int		j;

	i = 0;
	map->z_pow += x;
	if (map->z_pow > 6.283185307)
		map->z_pow -= 6.283185307;
	x = map->z_pow;
	centr(map);
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->cpy_tab[i][j].x = map->cpy_tab[i][j].x *
			cos(x) - map->cpy_tab[i][j].y * sin(x);
			map->cpy_tab[i][j].y = -map->cpy_tab[i][j].x * sin(x) +
			map->cpy_tab[i][j].y * cos(x);
			j++;
		}
		i++;
	}
	centr2(map);
}
