/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/06 03:19:25 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:30:34 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void	press9(int button, t_mlx *map, int i, int j)
{
	if (button == 53)
	{
		mlx_destroy_window(map->mlx, map->win);
		exit(1);
	}
	if (button == 35)
	{
		mlx_clear_window(map->mlx, map->win);
		if (map->perspect == 0)
		{
			map->perspect = 1;
			configure_output(map->cpy_tab, map);
		}
		else
		{
			map->perspect = 0;
			configure_output(map->cpy_tab, map);
		}
	}
}

void	press8(int button, t_mlx *map, int i, int j)
{
	if (button == 125)
	{
		map->move_down += 5;
		mlx_clear_window(map->mlx, map->win);
		configure_output(map->cpy_tab, map);
	}
	if (button == 126)
	{
		map->move_up += 5;
		mlx_clear_window(map->mlx, map->win);
		configure_output(map->cpy_tab, map);
	}
	if (button == 123)
	{
		map->move_left += 5;
		mlx_clear_window(map->mlx, map->win);
		configure_output(map->cpy_tab, map);
	}
	if (button == 124)
	{
		map->move_right += 5;
		mlx_clear_window(map->mlx, map->win);
		configure_output(map->cpy_tab, map);
	}
}

void	press7(int button, t_mlx *map, int i, int j)
{
	if (button == 8)
	{
		i = 0;
		while (i < map->nbr_line)
		{
			j = 0;
			while (j < map->len_line)
			{
				map->cpy_tab[i][j].x = map->tab[i][j].x;
				map->cpy_tab[i][j].y = map->tab[i][j].y;
				map->cpy_tab[i][j].z = map->tab[i][j].z;
				j++;
			}
			i++;
		}
		mlx_clear_window(map->mlx, map->win);
		rot_x(-0.05, map);
		rot_y(0, map);
		rot_z(0, map);
		configure_output(map->cpy_tab, map);
	}
}

void	press6(int button, t_mlx *map, int i, int j)
{
	if (button == 32)
	{
		i = 0;
		while (i < map->nbr_line)
		{
			j = 0;
			while (j < map->len_line)
			{
				map->cpy_tab[i][j].x = map->tab[i][j].x;
				map->cpy_tab[i][j].y = map->tab[i][j].y;
				map->cpy_tab[i][j].z = map->tab[i][j].z;
				j++;
			}
			i++;
		}
		mlx_clear_window(map->mlx, map->win);
		rot_x(0, map);
		rot_y(-0.05, map);
		rot_z(0, map);
		configure_output(map->cpy_tab, map);
	}
}

void	press5(int button, t_mlx *map, int i, int j)
{
	if (button == 0)
	{
		i = 0;
		while (i < map->nbr_line)
		{
			j = 0;
			while (j < map->len_line)
			{
				map->cpy_tab[i][j].x = map->tab[i][j].x;
				map->cpy_tab[i][j].y = map->tab[i][j].y;
				map->cpy_tab[i][j].z = map->tab[i][j].z;
				j++;
			}
			i++;
		}
		mlx_clear_window(map->mlx, map->win);
		rot_x(0, map);
		rot_y(0, map);
		rot_z(-0.05, map);
		configure_output(map->cpy_tab, map);
	}
}
