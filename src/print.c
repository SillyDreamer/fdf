/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 17:45:21 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:29:01 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void	ft_print_nice_int_tab(t_fdf *mas)
{
	int i;

	if (!mas)
		return ;
	i = 0;
	ft_putstr("\n");
	while (i < 19)
	{
		ft_putnbr(mas[i].x);
		ft_putstr("|");
		ft_putnbr(mas[i].y);
		ft_putstr("|");
		ft_putnbr(mas[i].z);
		ft_putstr("|");
		ft_putnbr(mas[i].color);
		ft_putstr(" ");
		i++;
	}
	ft_putstr("\n");
}
