/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 17:39:18 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:29:34 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void	move_down(t_mlx *map)
{
	int		i;
	int		j;

	i = 0;
	mlx_clear_window(map->mlx, map->win);
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->tab[i][j].y += 5;
			map->cpy_tab[i][j].y += 5;
			j++;
		}
		i++;
	}
	ft_drow(map->cpy_tab, map);
}

void	move_up(t_mlx *map)
{
	int		i;
	int		j;

	i = 0;
	mlx_clear_window(map->mlx, map->win);
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->tab[i][j].y -= 5;
			map->cpy_tab[i][j].y -= 5;
			j++;
		}
		i++;
	}
	ft_drow(map->cpy_tab, map);
}

void	move_left(t_mlx *map)
{
	int		i;
	int		j;

	i = 0;
	mlx_clear_window(map->mlx, map->win);
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->tab[i][j].x -= 5;
			map->cpy_tab[i][j].x -= 5;
			j++;
		}
		i++;
	}
	ft_drow(map->cpy_tab, map);
}

void	move_right(t_mlx *map)
{
	int		i;
	int		j;

	i = 0;
	mlx_clear_window(map->mlx, map->win);
	while (i < map->nbr_line)
	{
		j = 0;
		while (j < map->len_line)
		{
			map->tab[i][j].x += 5;
			map->cpy_tab[i][j].x += 5;
			j++;
		}
		i++;
	}
	ft_drow(map->cpy_tab, map);
}
