#include <stdlib.h>
#include "mlx.h"
#include <stdio.h>

typedef struct  s_map 
{
	void	*mlx_ptr;
	void	*win_ptr;
	int		i;
	int		j;
	int		x;
	int		y;
}               t_map;


int main(int ac, char **av)
{
    t_map   *map;
    int     ret;
    char    *line;
    char    **arr;
    char    **matrix;
    int i;
    int j;

    i = 0;
    if (ac != 2)
        return (0);
    while (ret = get_next_line(av[1], &line) > 0)
    {
        arr = ft_strsplit(line, ' ');
        j = 0;
        while(j++)
            matrix[i][j] = atoi(arr[j]);
        i++;
    }

}