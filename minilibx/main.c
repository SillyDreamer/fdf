
#include <stdlib.h>
#include "mlx.h"
#include <stdio.h>
#define ABS(x) (x < 0) ? -(x) : x

typedef struct  s_map 
{
	void	*mlx_ptr;
	void	*win_ptr;
	int		i;
	int		j;
	int		x;
	int		y;
}				t_map;


int deal_key(int key, t_map *map)
	{


		if (key == 53)
		{
			mlx_destroy_window(map->mlx_ptr, map->win_ptr);
			exit (1);
		}
		return (0);
	}


void		ft_putline(int x0, int y0, int x1, int y1)
{
	int			delta_x = ABS(x1 - x0);
	int			delta_y = ABS(y1 - y0);
	int			sign_x = (x0 < x1) ? 1 : -1;
	int			sign_y = (y0 < y1) ? 1 : -1;
	void		*mlx_ptr;
	void		*win_ptr;

	int			error = delta_x - delta_y;
	int			error2;

	mlx_ptr = mlx_init();
	win_ptr = mlx_new_window(mlx_ptr, 500, 500, "krya");
	while(x0 != x1 || y0 != y1)
   {
        mlx_pixel_put(mlx_ptr, win_ptr, x0, y0, 0x000000ff);
		error2 = error * 2;
        if(error2 > -delta_y) 
        {
            error -= delta_y;
            x0 += sign_x;
        }
        if(error2 < delta_x) 
        {
            error += delta_x;
            y0 += sign_y;
        }
    }
	mlx_pixel_put(mlx_ptr, win_ptr, x1, y1, 0x00ff0000);
	mlx_key_hook(win_ptr, deal_key, (void *)53);
	mlx_loop(mlx_ptr);
}



int press (int button, t_map *map)
{
	if (button == 53)
		{
			mlx_destroy_window(map->mlx_ptr, map->win_ptr);
			exit (0);
		}
	if (button == 124)
		{
			map->j++;
			mlx_pixel_put(map->mlx_ptr, map->win_ptr, map->j, map->i, 0x0000ff);
		}
	else if (button == 125)
		{
			map->i++;
			mlx_pixel_put(map->mlx_ptr, map->win_ptr, map->j, map->i, 0x00ff00);
		}
	else if (button == 126)
	{
		map->i--;
		mlx_pixel_put(map->mlx_ptr, map->win_ptr, map->j, map->i, 0xff0000);
	}
	else if (button == 123)
		{
			map->j--;
			mlx_pixel_put(map->mlx_ptr, map->win_ptr, map->j, map->i, 0x00ffff);
		}
	if (button == 12)
	{
		mlx_clear_window(map->mlx_ptr, map->win_ptr);
	}
	return (0);
}
int	press_mouse (int button, int x, int y, t_map *map)
{
	if (button == 1)
		{
			//map->i = map->i + 5;
			//map->j = map->j + 5;
			mlx_string_put(map->mlx_ptr, map->win_ptr, map->i, map->j, 0x00ff0000, "Hello");
		}
	return (0);
}

int main()
{
	void	*mlx_ptr;
	void	*win_ptr;

	t_map		*map;


	mlx_ptr = mlx_init();
	win_ptr = mlx_new_window(mlx_ptr, 650, 500, "krya");
	map = (t_map *)malloc(sizeof(t_map));
	//if (map)
	//{
	//	map->mlx_ptr = mlx_ptr;
	//	map->win_ptr = win_ptr;
	//}
	map->mlx_ptr = mlx_ptr;
	map->win_ptr = win_ptr;
	//int res = mlx_key_hook(map->win_ptr, deal_key, map);
	//printf(">>> %d\n", res);
	//map = malloc(sizeof(t_map));
	//mlx_pixel_put(mlx_ptr, win_ptr, 200, 200, 0x00ffffff);
	//mlx_string_put(mlx_ptr, win_ptr, 200, 200, 0x00ff0000, s);
	

	mlx_hook(win_ptr, 2, 0, press, map);
	mlx_hook(win_ptr, 4, 0, press_mouse, map);
	//int	mlx_hook(t_win_list *win, int x_event, int x_mask, int (*funct)(),void *param)
	//ft_putline(0, 0, 650, 500);
	mlx_loop(mlx_ptr);
	return (0);
}
