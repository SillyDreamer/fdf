Compiling
Run the following commands:

To compile:

make

To remove objects:

make clean

To remove objects and binary file (program):

make fclean

To re-compile:

make re


Executing
To execute the program:

./fdf <fdf-file>

Try running the following:
./fdf maps/test_maps/42.fdf

./fdf maps/test_maps/mars.fdf

./fdf maps/personal/t1.fdf