NAME = fdf

INC = /usr/local/include

LIB = /usr/local/lib

FLAGS = -lmlx -framework OpenGL -framework AppKit

LIBFT = libft/libft.a

FILES_C = src/main.c src/main2.c src/get_next_line.c src/print.c src/putline.c src/key.c src/ft_move.c src/ft_rotation.c src/atoi_base.c src/ft_drow2.c src/key2.c

FILES_O = atoi_base.o ft_drow2.o ft_move.o ft_rotation.o get_next_line.o key.o key2.o main.o main2.o print.o putline.o

all: $(NAME)

$(NAME):
		gcc -c $(FILES_C)
		gcc  -o $(NAME) -I $(INC) $(FILES_C) $(LIBFT) -L $(LIB) $(FLAGS)

clean:
	rm -Rf $(FILES_O)
fclean:
	make clean
	rm -Rf $(NAME)
re:
	make fclean
	make
