/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 18:58:31 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/30 20:56:25 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static void		d(void *d, size_t n)
{
	free(d);
	(void)n;
}

t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*my;
	t_list	*my_dump;
	t_list	*prev;

	if (!lst || !f)
		return (NULL);
	if (!(my = malloc(sizeof(t_list))))
		return (NULL);
	my = f(lst);
	lst = lst->next;
	prev = my;
	while (lst)
	{
		if (!(my_dump = f(lst)))
		{
			ft_lstdel(&my, d);
			return (NULL);
		}
		lst = lst->next;
		if (prev)
			prev->next = my_dump;
		prev = my_dump;
		my_dump = my_dump->next;
	}
	return (my);
}
