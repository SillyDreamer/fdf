/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 19:21:06 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/03 18:39:01 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strnew(size_t size)
{
	char			*tmp;
	unsigned long	i;

	i = 0;
	tmp = (char*)malloc(size + 1);
	if (!tmp)
		return (NULL);
	while (i < size + 1)
	{
		tmp[i] = 0;
		i++;
	}
	return (tmp);
}
