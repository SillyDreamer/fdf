/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_matrix.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 23:34:09 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/03 23:50:37 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static void	clean(int **tab, int n)
{
	int i;

	if (tab && n)
	{
		i = 0;
		while (i < n)
		{
			free(tab[i]);
			i++;
		}
	}
}

int			**ft_set_matrix(int n, int m, int val)
{
	int **res;
	int i;
	int j;

	i = 0;
	if (!(res = (int**)malloc(sizeof(int*) * n + 1)))
		return (NULL);
	while (i < n)
	{
		j = 0;
		if (!(res[i] = (int*)malloc(sizeof(int) * m)))
		{
			clean(res, i);
			return (NULL);
		}
		while (j < m)
		{
			res[i][j] = val;
			j++;
		}
		i++;
	}
	return (res);
}
