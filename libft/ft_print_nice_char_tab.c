/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nice_char_tab.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 23:23:40 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/03 23:25:51 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_print_nice_char_tab(char **mas, int n)
{
	int i;

	if (!n || !mas)
		return ;
	i = 0;
	ft_putstr("\n[");
	while (i < n && mas[i])
	{
		ft_putstr(mas[i]);
		i++;
		if (i != n)
			ft_putstr(", ");
	}
	ft_putstr("]\n");
}
