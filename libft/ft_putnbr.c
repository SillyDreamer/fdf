/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 19:13:11 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/30 22:36:43 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_post_putnbr(int n)
{
	int neg;

	if (!n)
		return ;
	neg = 1;
	if (n < 0)
	{
		ft_putchar('-');
		neg = -1;
	}
	if (n > 9 || n < -9)
		ft_putnbr((n / 10) * neg);
	ft_putchar('0' + (n % 10) * neg);
}

void		ft_putnbr(int n)
{
	if (!n)
		ft_putchar('0');
	else
		ft_post_putnbr(n);
}
