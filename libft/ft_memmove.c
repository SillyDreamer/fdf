/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 19:15:58 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/03 18:32:38 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static void	ft_nach(void *dest, const void *src, size_t n)
{
	unsigned long	i;
	char			*tmp_dest;
	char			*tmp_src;

	tmp_dest = (char*)dest;
	tmp_src = (char*)src;
	i = 0;
	while (i < n)
	{
		tmp_dest[i] = tmp_src[i];
		i++;
	}
}

static void	ft_kon(void *dest, const void *src, size_t n)
{
	char			*tmp_dest;
	char			*tmp_src;

	tmp_dest = (char*)dest;
	tmp_src = (char*)src;
	while (n--)
		tmp_dest[n] = tmp_src[n];
}

void		*ft_memmove(void *dest, const void *src, size_t n)
{
	char			*tmp_dest;
	char			*tmp_src;

	tmp_dest = (char*)dest;
	tmp_src = (char*)src;
	if (!n || dest == src)
		return (dest);
	if (tmp_dest < tmp_src)
		ft_nach(dest, src, n);
	else
		ft_kon(dest, src, n);
	return (dest);
}
