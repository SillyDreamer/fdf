/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 23:54:10 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/04 18:00:45 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int	isdigit(int c)
{
	char s;

	s = (char)c;
	if (s >= '0' && s <= '9')
		return (1);
	return (0);
}

static int	is_spasess(char c)
{
	return (c == '\n' || c == '\t'
			|| c == ' ' || c == '\f' || c == '\r' || c == '\v');
}

int			ft_atoi(const char *str)
{
	int			i;
	long int	res;
	int			n;

	i = 0;
	res = 0;
	n = 0;
	while (is_spasess(str[i]) && str[i])
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			n += 1;
		i++;
	}
	while (!is_spasess(str[i]) && isdigit(str[i]) && str[i] && res >= 0)
		res = res * 10 + (str[i++] - '0');
	if (res < 0)
		return (-1 + n);
	if (n)
		res *= -1;
	return ((int)res);
}
