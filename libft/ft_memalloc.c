/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 21:01:04 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/30 21:01:16 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>

void	*ft_memalloc(size_t size)
{
	void			*mem;
	char			*tmp;
	unsigned long	i;

	i = 0;
	if (!size)
		return (0);
	mem = (void*)malloc(size);
	if (!mem)
		return (NULL);
	tmp = (char*)mem;
	while (i < size)
	{
		tmp[i] = 0;
		i++;
	}
	return (mem);
}
