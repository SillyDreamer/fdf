/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 22:58:12 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/24 23:43:33 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static int	set_end(char const *str)
{
	int i;
	int res;

	res = 0;
	i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
			res = i;
		i++;
	}
	return (res);
}

static int	kostil(char const *str, int x)
{
	int i;
	int res;

	res = 0;
	i = 0;
	while (str[i] && (str[i] == ' ' || str[i] == '\t' || str[i] == '\n'))
		i++;
	while (i < x)
	{
		res++;
		i++;
	}
	return (res);
}

char		*ft_strtrim(char const *s)
{
	int		i;
	int		j;
	char	*str;

	j = 0;
	i = 0;
	if (!s)
		return (NULL);
	str = (char*)malloc(kostil(s, set_end(s)) + 2);
	if (!str)
		return (NULL);
	while (s[i] && (s[i] == ' ' || s[i] == '\t' || s[i] == '\n'))
		i++;
	while (i < set_end(s) + 1)
	{
		str[j] = s[i];
		i++;
		j++;
	}
	str[j] = '\0';
	return (str);
}
