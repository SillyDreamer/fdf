/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 22:50:46 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/21 22:50:47 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

static char		*ft_strstr2(char *str, char *to_find)
{
	int dump;
	int i;
	int j;

	i = 0;
	dump = 0;
	while (str[i] != '\0')
	{
		i = dump;
		dump = i;
		j = 0;
		while (str[i] == to_find[j] && to_find[j] != '\0')
		{
			i++;
			j++;
			if (!to_find[j])
				return (&str[dump]);
		}
		dump++;
	}
	return (NULL);
}

char			*ft_strstr(const char *haystack, const char *needle)
{
	char *str;
	char *to_find;

	str = (char*)haystack;
	to_find = (char*)needle;
	if (to_find[0] == '\0')
		return (&str[0]);
	if (ft_strstr2(str, to_find))
		return (ft_strstr2(str, to_find));
	return ((char *)0);
}
