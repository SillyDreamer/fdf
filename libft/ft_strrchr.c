/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 22:39:27 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/04 16:09:11 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*tmp;

	tmp = (char*)s;
	i = ft_strlen(s);
	while (i != -1)
	{
		if (tmp[i] == (char)c)
			return (&tmp[i]);
		i--;
	}
	return (NULL);
}
