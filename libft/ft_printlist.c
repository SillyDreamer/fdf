/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printlist.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 23:07:55 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/03 23:17:30 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_printlist(t_list *lst)
{
	char *str;

	if (lst)
	{
		str = (char*)lst->content;
		ft_putstr(str);
		ft_putstr("\n");
	}
	else
		return ;
	if (lst->next)
		ft_printlist(lst->next);
}
