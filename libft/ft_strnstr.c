/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 23:35:18 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/04 17:15:41 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

static char	*ft_strstr(char *s1, char *s2, char *tmp_s1)
{
	size_t i;

	if (s1[0])
	{
		i = 0;
		while (s1[i] == s2[i] && s1[i] && s2[i] && &s1[i] < tmp_s1)
			i++;
		if (!s2[i])
			return (s1);
		else
			return (ft_strstr(&s1[1], s2, tmp_s1));
	}
	return (NULL);
}

char		*ft_strnstr(char *s1, char *s2, size_t len)
{
	return (ft_strstr(s1, s2, &s1[len]));
}
