/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 21:08:29 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/27 17:17:39 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	if (!alst)
		return ;
	if (alst[0]->next)
		ft_lstdel(&alst[0]->next, del);
	if ((alst[0])->content != NULL)
		del((alst[0])->content, (alst[0])->content_size);
	(alst[0])->content_size = 0;
	alst[0]->next = NULL;
	free(alst[0]);
	*alst = NULL;
}
