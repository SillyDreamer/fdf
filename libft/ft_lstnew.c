/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 19:19:28 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/26 20:39:19 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*list;
	void	*tmp_data;

	if ((list = (t_list*)malloc(sizeof(t_list))))
	{
		if (content_size && !(tmp_data = ft_memalloc(content_size)))
		{
			free(list);
			return (NULL);
		}
		if (!content || !content_size)
		{
			list->content = NULL;
			list->content_size = 0;
			list->next = NULL;
			return (list);
		}
		ft_memcpy(tmp_data, content, content_size);
		list->content = tmp_data;
		list->content_size = content_size;
		list->next = NULL;
		return (list);
	}
	return (NULL);
}
