/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 18:15:00 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/25 23:25:04 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

static int	len(int n, int *neg)
{
	int res;

	res = 0;
	if (n < 0)
	{
		*neg = 1;
		res++;
	}
	if (n == 0)
		return (1);
	while (n)
	{
		n /= 10;
		res++;
	}
	return (res);
}

static char	*ft_itoa2(long int n, int i, int neg, char *res)
{
	int		dump_len;
	int		leng;

	dump_len = len(n, &neg);
	res[dump_len] = '\0';
	leng = len(n, &neg) - 1;
	if (neg)
	{
		n *= -1;
		res[0] = '-';
	}
	while (i < dump_len - neg)
	{
		res[leng--] = '0' + (n % 10);
		n /= 10;
		i++;
	}
	return (res);
}

char		*ft_itoa(int n)
{
	char		*res;
	int			neg;
	int			i;
	long int	tmp_n;

	tmp_n = (int)n;
	i = 0;
	neg = 0;
	res = (char*)malloc(len(n, &neg) + 1);
	if (!(res = (char*)malloc(len(n, &neg) + 1)))
		return (NULL);
	return (ft_itoa2(n, i, neg, res));
}
