/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 19:22:22 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/25 20:03:55 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_post_putnbr_fd(int n, int fd)
{
	int neg;

	if (!n)
		return ;
	neg = 1;
	if (n < 0)
	{
		ft_putchar_fd('-', fd);
		neg = -1;
	}
	if (n > 9 || n < 9)
		ft_post_putnbr_fd((n / 10) * neg, fd);
	ft_putchar_fd('0' + (n % 10) * neg, fd);
}

void		ft_putnbr_fd(int n, int fd)
{
	if (!n)
		ft_putchar_fd('0', fd);
	else
		ft_post_putnbr_fd(n, fd);
}
