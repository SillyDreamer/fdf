/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 19:15:38 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/11/23 21:23:31 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned long	i;
	char			*tmp_dest;
	char			*tmp_src;
	char			tmp_c;

	tmp_c = (char)c;
	tmp_dest = (char*)dst;
	tmp_src = (char*)src;
	i = 0;
	while (i < n)
	{
		tmp_dest[i] = tmp_src[i];
		if (tmp_dest[i] == tmp_c)
			return (&tmp_dest[i + 1]);
		i++;
	}
	return (NULL);
}
