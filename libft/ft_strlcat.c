/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 21:36:35 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/04 17:20:53 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

static size_t	ft_ft_strlen(const char *str2)
{
	size_t j;

	j = 0;
	while (str2[j])
		j++;
	return (j);
}

static size_t	ft_leng(char *str)
{
	size_t i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

size_t			ft_strlcat(char *dst, const char *src, size_t size)
{
	unsigned long	i;
	unsigned long	j;
	size_t			tmp_size;
	size_t			res;

	tmp_size = (size_t)size;
	if (ft_leng(dst) < tmp_size)
		res = ft_leng(dst) + ft_ft_strlen(src);
	else
		res = size + ft_ft_strlen(src);
	i = 0;
	j = 0;
	while (dst[i])
		i++;
	while (src && src[j] && size && i < size - 1 && size)
	{
		dst[i] = src[j];
		i++;
		j++;
	}
	dst[i] = '\0';
	return (res);
}
