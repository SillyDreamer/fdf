/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 17:21:11 by bbaelor-          #+#    #+#             */
/*   Updated: 2018/12/04 18:19:44 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	len_mas(char const *str, char c)
{
	int i;
	int res;

	i = 0;
	res = 0;
	if (!str)
		return (0);
	while (str[i])
	{
		while (str[i] && str[i] == c)
			i++;
		if (!str[i] && !res)
			return (-1);
		if (str[i])
			res++;
		while (str[i] && str[i] != c)
			i++;
	}
	return (res);
}

static void	clean_memory(char **res)
{
	int i;

	i = 0;
	while (res[i])
	{
		free(res[i]);
		res[i] = NULL;
		i++;
	}
	free(res);
	res = NULL;
	return ;
}

static int	len_word(char const *str, char c)
{
	int i;
	int res;

	i = 0;
	res = 0;
	while (str[i] && str[i] == c)
		i++;
	while (str[i] && str[i] != c)
	{
		res++;
		i++;
	}
	return (res);
}

static char	**ft_strsplit2(char const *s, char c)
{
	char	**res;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (!s || !(res = (char **)malloc(sizeof(char *) * len_mas(s, c) + 1)))
		return (NULL);
	while (i < len_mas(s, c) && s[j])
	{
		while (s[j] && s[j] == c)
			j++;
		if (!(res[i] = (char *)malloc(sizeof(char) * (len_word(s + j, c) + 1))))
		{
			clean_memory(res);
			return (NULL);
		}
		ft_strncpy(res[i], s + j, len_word(s + j, c));
		res[i][len_word(s + j, c)] = '\0';
		j += len_word(s + j, c);
		i++;
	}
	res[i] = 0;
	return (res);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**res;

	if (((s) && (!s[0])) || len_mas(s, c) == -1)
	{
		res = (char **)malloc(sizeof(char *) * 1);
		if (!res)
			return (NULL);
		res[0] = NULL;
		return (res);
	}
	return (ft_strsplit2(s, c));
}
