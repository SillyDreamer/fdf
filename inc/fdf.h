/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 18:03:37 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/02/27 05:27:18 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H

# define FDF_H

# include <unistd.h>
# include <math.h>
# include <stdio.h>
# include <stdlib.h>
# include "../libft/libft.h"
# include "get_next_line.h"
# include "../minilibx/mlx.h"

typedef struct		s_fdf
{
	double			x;
	double			y;
	double			z;
	double			x_curent;
	double			y_curent;
	double			delta_x;
	double			delta_y;
	int				color;
}					t_fdf;

typedef struct		s_mlx
{
	int				nbr_line;
	int				len_line;
	void			*mlx;
	void			*win;
	t_fdf			**tab;
	t_fdf			**cpy_tab;
	int				color;
	int				x_centr;
	int				y_centr;
	double			x_pow;
	double			y_pow;
	double			z_pow;
	int				scale;
	int				check_len;
	int				move_down;
	int				move_up;
	int				move_left;
	int				move_right;
	int				perspect;
}					t_mlx;

void				ft_print_nice_int_tab(t_fdf *mas);
void				ft_drow(t_fdf **fdf, t_mlx *mlx);
int					press (int button, t_mlx *map);
void				ft_count_line(t_mlx *fdf, t_fdf **arr);
void				ft_len_line(t_fdf ***fdf, t_mlx *mlx);
void				rot_x(double x, t_mlx *map);
void				rot_y(double x, t_mlx *map);
void				rot_z(double x, t_mlx *map);
void				move_down(t_mlx *map);
void				move_left(t_mlx *map);
void				move_right(t_mlx *map);
void				move_up(t_mlx *map);
int					get_color(t_fdf c, t_fdf start, t_fdf end);
int					ft_atoi_base(const char *str, int str_base);
void				ft_putline(t_fdf fdf, t_fdf fdf2, t_mlx *mlx, int color);
void				ft_drow2(t_fdf **fdf, t_mlx *mlx);
void				press9(int button, t_mlx *map, int i, int j);
void				press8(int button, t_mlx *map, int i, int j);
void				press7(int button, t_mlx *map, int i, int j);
void				press6(int button, t_mlx *map, int i, int j);
void				press5(int button, t_mlx *map, int i, int j);
void				configure_output(t_fdf **fdf, t_mlx *mlx);
void				count_line2(t_mlx *mlx, int fd);
void				count_line(t_fdf **arr, t_mlx *mlx);
int					tab_len(char **tab, t_mlx *mlx);
void				check_valid(t_mlx *fdf, char **str);
void				check_valid2(int argc, char **argv);
void				ft_exit(char *str);

#endif
